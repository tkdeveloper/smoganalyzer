package com.ath.smoganalyzer.repositories

import android.content.SharedPreferences
import com.ath.smoganalyzer.repositories.sharedPreferences.SharedPreferenceRealRepository
import com.ath.smoganalyzer.repositories.sharedPreferences.SharedPreferencesRepository
import io.reactivex.rxjava3.observers.TestObserver
import io.reactivex.rxjava3.plugins.RxJavaPlugins
import io.reactivex.rxjava3.schedulers.Schedulers
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith
import org.mockito.Mockito.mock
import org.mockito.junit.MockitoJUnitRunner

@RunWith(MockitoJUnitRunner.Silent::class)
class SharedPreferencesRepositoryTest {

    private lateinit var sharedPreferencesRepository: SharedPreferencesRepository

    @Before
    fun setup() {
        val sharedPref = mock(SharedPreferences::class.java)
        sharedPreferencesRepository = SharedPreferenceRealRepository(sharedPref)
        RxJavaPlugins.setErrorHandler { throwable -> }
    }

    @Test
    fun shouldReturnNotFirstTime() {
        val testObserver: TestObserver<Boolean> = TestObserver()
        sharedPreferencesRepository.checkIfFirstTime().subscribe(testObserver)

        testObserver.assertValue(false)
    }

    @Test
    fun shouldReturnNotDownloaded() {
        val testObserver: TestObserver<Boolean> = TestObserver()
        sharedPreferencesRepository.getDownloadCompleted().subscribe(testObserver)

        testObserver.assertValue(false)
    }
}