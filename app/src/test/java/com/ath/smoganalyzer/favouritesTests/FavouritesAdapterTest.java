package com.ath.smoganalyzer.favouritesTests;

import com.ath.smoganalyzer.model.FavouriteStation;
import com.ath.smoganalyzer.ui.favourites.FavouritesAdapter;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.util.ArrayList;

public class FavouritesAdapterTest {

    private FavouritesAdapter favouritesAdapter;

    @Before
    public void setup() {
        // init mocks
        favouritesAdapter = new FavouritesAdapter();
    }

    @Test
    public void addNewItem() {
        FavouriteStation station = new FavouriteStation(2, "test");
        favouritesAdapter.addSingleElement(station);
        Assert.assertTrue(favouritesAdapter.getItemCount() >= 1);
    }

    @Test
    public void addMultipleItems() {
        ArrayList<FavouriteStation> list = new ArrayList<FavouriteStation>();
        list.add(new FavouriteStation(1, "test1"));
        list.add(new FavouriteStation(2, "test2"));
        favouritesAdapter.addMultipleItems(list);
        Assert.assertTrue(favouritesAdapter.getItemCount() > 1);
    }

    @Test
    public void removeData() {
        FavouriteStation station = new FavouriteStation(1, "test1");
        FavouriteStation station2 = new FavouriteStation(2, "test2");
        favouritesAdapter.addSingleElement(station);
        favouritesAdapter.addSingleElement(station2);
        favouritesAdapter.removeDataFromList(station);
        Assert.assertTrue(favouritesAdapter.getItemCount() == 1);
    }

    @Test
    public void removeAllData() {
        FavouriteStation station = new FavouriteStation(1, "test1");
        FavouriteStation station2 = new FavouriteStation(2, "test2");
        favouritesAdapter.addSingleElement(station);
        favouritesAdapter.addSingleElement(station2);
        favouritesAdapter.removeAllData();
        Assert.assertTrue(favouritesAdapter.getItemCount() == 0);
    }

    @Test
    public void reloadData() {
        ArrayList<FavouriteStation> list = new ArrayList<FavouriteStation>();
        list.add(new FavouriteStation(1, "test1"));
        list.add(new FavouriteStation(2, "test2"));
        favouritesAdapter.addMultipleItems(list);
        favouritesAdapter.reloadAllData(list);
        Assert.assertTrue(favouritesAdapter.getItemCount() == 2);
    }

}
