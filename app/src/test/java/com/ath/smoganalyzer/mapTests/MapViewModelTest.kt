package com.ath.smoganalyzer.mapTests

import com.ath.smoganalyzer.model.Station
import com.ath.smoganalyzer.ui.map.MapViewModel
import com.google.android.gms.maps.model.LatLng
import com.google.android.gms.maps.model.MarkerOptions
import junit.framework.Assert.assertEquals
import org.junit.Before
import org.junit.Test
import org.mockito.Mock
import org.mockito.MockitoAnnotations

class MapViewModelTest {

    private lateinit var stations: ArrayList<Station>
    private val latitude = "50.458989"
    private val longitude = "17.331906"
    private val stationName = "StationLabel"

    @Mock
    lateinit var mapViewModel: MapViewModel

    @Before
    fun setup() {
        MockitoAnnotations.initMocks(this)
    }

    @Test
    fun `Should convert into list of markers`() {
        val sensor = Station(1, stationName, latitude, longitude)
        val sensorMarker = MarkerOptions().position(
            LatLng(50.458989, 17.331906)
        ).title(stationName)

        val result = mapViewModel.convertSensorIntoMarker(sensor)

        assertEquals("Title", result.title, sensorMarker.title)
        assertEquals("Lat", result.position.latitude, sensorMarker.position.latitude)
        assertEquals("Long", result.position.longitude, sensorMarker.position.longitude)
    }

    private fun fillSensorList() {
        for (i in 1..10) {
            val sensor = Station(1, "", latitude, longitude)
            stations.add(sensor)
        }
    }
}