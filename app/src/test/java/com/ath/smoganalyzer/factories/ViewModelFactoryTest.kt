package com.ath.smoganalyzer.factories

import com.ath.smoganalyzer.application.ViewModelFactory
import com.ath.smoganalyzer.ui.map.MapViewModel
import org.junit.Before
import org.junit.Test
import org.mockito.Mock
import org.mockito.MockitoAnnotations

class ViewModelFactoryTest {

    @Mock
    lateinit var viewModelFactory: ViewModelFactory

    @Before
    fun setup() {
        MockitoAnnotations.initMocks(this)
    }

    @Test
    fun `Should return mapViewModel`() {
        val mapViewModel = MapViewModel()
        val createdViewModel = viewModelFactory.create(mapViewModel::class.java)
        assert(mapViewModel == createdViewModel)
    }
}