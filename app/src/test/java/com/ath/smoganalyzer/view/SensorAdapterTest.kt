package com.ath.smoganalyzer.view

import com.ath.smoganalyzer.model.StationSensorsAllDataUI
import com.ath.smoganalyzer.ui.activities.station.recycler.SensorAdapter
import org.junit.Before
import org.junit.Test

class SensorAdapterTest {

    private lateinit var sensorAdapter: SensorAdapter

    @Before
    fun setUp() {
        sensorAdapter = SensorAdapter()
    }

    @Test
    fun `Should return filled list`() {
        val firstStation = StationSensorsAllDataUI(null, null)
        sensorAdapter.addNewSensorData(firstStation)

        assert(sensorAdapter.itemCount != 0)
    }
}