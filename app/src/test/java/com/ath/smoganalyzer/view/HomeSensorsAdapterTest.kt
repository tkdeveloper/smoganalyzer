package com.ath.smoganalyzer.view

import com.ath.smoganalyzer.model.StationSensorsAllDataUI
import com.ath.smoganalyzer.ui.home.recycler.HomeSensorsAdapter
import org.junit.Before
import org.junit.Test
import org.mockito.MockitoAnnotations

class HomeSensorsAdapterTest {

    private lateinit var homeSensorsAdapter: HomeSensorsAdapter

    @Before
    fun setUp() {
        MockitoAnnotations.initMocks(this)

        homeSensorsAdapter = HomeSensorsAdapter()
    }

    @Test
    fun `Should return filled list`() {
        val firstStation = StationSensorsAllDataUI(null, null)
        homeSensorsAdapter.addNewSensorData(firstStation)

        assert(homeSensorsAdapter.itemCount != 0)
    }
}