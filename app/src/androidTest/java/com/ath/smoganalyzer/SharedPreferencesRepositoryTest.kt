package com.ath.smoganalyzer

import android.content.Context
import androidx.test.platform.app.InstrumentationRegistry
import androidx.test.runner.AndroidJUnit4
import com.ath.smoganalyzer.repositories.sharedPreferences.SharedPreferenceRealRepository
import io.reactivex.rxjava3.observers.TestObserver
import io.reactivex.rxjava3.plugins.RxJavaPlugins
import io.reactivex.rxjava3.schedulers.Schedulers
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith

@RunWith(AndroidJUnit4::class)
class SharedPreferencesRepositoryTest {

    lateinit var sharedPreferencesRepository: SharedPreferenceRealRepository

    @Before
    fun setup() {
        val context = InstrumentationRegistry.getInstrumentation().context
        val sharedPreferences = context.getSharedPreferences(
            "PrefSmogRef",
            Context.MODE_PRIVATE
        )
        sharedPreferencesRepository = SharedPreferenceRealRepository(sharedPreferences)
        RxJavaPlugins.setErrorHandler { throwable -> }
    }

    @Test
    fun shouldReturnFirstTime() {
        val testObserver: TestObserver<Boolean> = TestObserver()
        sharedPreferencesRepository.checkIfFirstTime().subscribe(testObserver)

        testObserver.assertValue(true)
    }

    @Test
    fun shouldReturnNotDownloaded() {
        val testObserver: TestObserver<Boolean> = TestObserver()
        sharedPreferencesRepository.getDownloadCompleted().subscribe(testObserver)

        testObserver.assertValue(false)
    }

    @Test
    fun shouldReturnDownloaded() {
        val testObserver: TestObserver<Boolean> = TestObserver()

        sharedPreferencesRepository.setDownloadCompleted().doOnComplete {
            sharedPreferencesRepository.getDownloadCompleted().subscribe(testObserver)
        }.subscribe()

        testObserver.assertValue(true)
    }

    @Test
    fun shouldSetNoHomeActive() {
        val testObserver: TestObserver<String> = TestObserver()
        sharedPreferencesRepository.getHomeActiveStation().subscribe(testObserver)

        testObserver.assertValue("")
    }

    @Test
    fun shouldSetHomeActive() {
        val testObserver: TestObserver<String> = TestObserver()

        sharedPreferencesRepository.saveHomeActiveStation("set")
            .doOnComplete {
                sharedPreferencesRepository.getHomeActiveStation()
                    .subscribe(testObserver)
            }
            .subscribe()

        testObserver.assertValue("set")
    }
}