package com.ath.smoganalyzer.database

import androidx.room.Room
import androidx.test.platform.app.InstrumentationRegistry
import androidx.test.runner.AndroidJUnit4
import com.ath.smoganalyzer.model.Station
import org.junit.After
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith

@RunWith(AndroidJUnit4::class)
open class StationDaoTest {
    private lateinit var stationDatabase: StationDatabase

    @Before
    fun setup() {
        stationDatabase = Room.inMemoryDatabaseBuilder(
            InstrumentationRegistry.getInstrumentation().context,
            StationDatabase::class.java
        ).build()
    }

    @After
    fun closeDb() {
        stationDatabase.close()
    }

    @Test
    fun insertStation() {
        val station = Station(0, "1", "2", "3")
        stationDatabase.stationDao()?.insertStation(station)
        val stationFromDb = stationDatabase.stationDao()?.findByStationName(station.stationName)

        assert(station == stationFromDb)
    }

    @Test
    fun insertManyStations() {
        val stations = arrayListOf<Station>()
        stations.add(Station(0, "1", "2", "3"))
        stations.add(Station(1, "2", "1", "3"))
        stations.add(Station(2, "3", "4", "3"))

        stationDatabase.stationDao()?.insertStation(stations)

        stations.forEach {
            val itemFromDb = stationDatabase.stationDao()?.findByStationName(it.stationName)
            assert(itemFromDb == it)
        }
    }

    @Test
    fun deleteFromDb() {
        val station = Station(0, "1", "2", "3")
        stationDatabase.stationDao()?.insertStation(station)
        stationDatabase.stationDao()?.deleteAll()

        val itemFromDb = stationDatabase.stationDao()?.findByStationName(station.stationName)

        assert(itemFromDb == null)
    }
}