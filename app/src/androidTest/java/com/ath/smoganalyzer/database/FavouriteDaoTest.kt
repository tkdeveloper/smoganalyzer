package com.ath.smoganalyzer.database

import androidx.room.Room
import androidx.test.platform.app.InstrumentationRegistry
import androidx.test.runner.AndroidJUnit4
import com.ath.smoganalyzer.model.FavouriteStation
import org.junit.After
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith

@RunWith(AndroidJUnit4::class)
class FavouriteDaoTest {
    private lateinit var stationDatabase: StationDatabase

    @Before
    fun setup() {
        stationDatabase = Room.inMemoryDatabaseBuilder(
            InstrumentationRegistry.getInstrumentation().context,
            StationDatabase::class.java
        ).build()
    }

    @After
    fun closeDb() {
        stationDatabase.close()
    }

    @Test
    fun insertFavourite() {
        val favouriteStation = FavouriteStation(1, "Fav1")
        stationDatabase.favouritesDao()?.insertFavStation(favouriteStation)
        val favStationFromDb =
            stationDatabase.favouritesDao()?.findByStationName(favouriteStation.stationTitle)

        assert(favouriteStation == favStationFromDb)
    }

    @Test
    fun deleteFromDb() {
        val favouriteStation = FavouriteStation(1, "Fav1")
        stationDatabase.favouritesDao()?.insertFavStation(favouriteStation)
        stationDatabase.favouritesDao()?.deleteStationFromFavourites(favouriteStation.stationTitle)

        val itemFromDb = stationDatabase.favouritesDao()
            ?.findByStationName(favouriteStation.stationTitle)
        assert(itemFromDb == null)
    }
}