package com.ath.smoganalyzer.database

import androidx.room.Room
import androidx.test.platform.app.InstrumentationRegistry
import androidx.test.runner.AndroidJUnit4
import org.junit.Test
import org.junit.runner.RunWith

@RunWith(AndroidJUnit4::class)
class StationDatabaseTest {
    lateinit var database: StationDatabase

    @Test
    fun checkIfDatabaseIsNotNull() {
        database = Room.inMemoryDatabaseBuilder(
            InstrumentationRegistry.getInstrumentation().context,
            StationDatabase::class.java
        ).build()

        assert(database.favouritesDao() != null)
    }
}