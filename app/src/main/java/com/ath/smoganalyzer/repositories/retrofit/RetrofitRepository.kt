package com.ath.smoganalyzer.repositories.retrofit

import com.ath.smoganalyzer.model.StationSensors
import com.ath.smoganalyzer.ui.activities.station.OnApiDataFetched

interface RetrofitRepository {

    fun downloadAllSensors()
    fun downloadInfoOneStation(id: Int, activity: OnApiDataFetched)
    fun downloadDataFromOneSensor(
        stationSensors: StationSensors,
        activity: OnApiDataFetched
    )
}