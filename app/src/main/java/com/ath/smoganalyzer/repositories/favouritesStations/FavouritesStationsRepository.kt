package com.ath.smoganalyzer.repositories.favouritesStations

import com.ath.smoganalyzer.model.FavouriteStation
import com.ath.smoganalyzer.ui.favourites.OnFavouritesDataFetched

interface FavouritesStationsRepository {

    fun insert(favouriteStation: FavouriteStation)

    fun getAllFavouritesStations(onFavouritesDataFetched: OnFavouritesDataFetched)

    fun getStationByTitle(
        title: String, onFavouritesDataFetched: OnFavouritesDataFetched
    )

    fun removeFromFavourites(favouriteStation: FavouriteStation)
}