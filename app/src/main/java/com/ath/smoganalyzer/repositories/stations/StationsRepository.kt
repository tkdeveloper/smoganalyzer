package com.ath.smoganalyzer.repositories.stations

import androidx.lifecycle.LiveData
import com.ath.smoganalyzer.model.Station
import com.ath.smoganalyzer.ui.map.OnDatabaseDataFetched

interface StationsRepository {

    fun insert(stations: List<Station>)

    fun insert(station: Station)

    fun getAllStations(): LiveData<List<Station>>

    fun getStationByStationName(titleIntent: String?, onDatabaseDataFetched: OnDatabaseDataFetched)
}