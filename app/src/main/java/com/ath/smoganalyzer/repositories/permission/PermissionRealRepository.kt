package com.ath.smoganalyzer.repositories.permission

import android.Manifest
import android.app.Activity
import android.app.AlertDialog
import android.content.Context
import android.content.pm.PackageManager
import androidx.core.app.ActivityCompat
import com.ath.smoganalyzer.R
import com.ath.smoganalyzer.ui.map.MapViewModel


class PermissionRealRepository : PermissionRepository {
    override fun checkBasicPermissionForMap(context: Context) {
        checkLocationPermission(context)
    }

    private fun checkLocationPermission(context: Context) {
        if (ActivityCompat.checkSelfPermission(
                context,
                Manifest.permission.ACCESS_FINE_LOCATION
            ) != PackageManager.PERMISSION_GRANTED
        ) {
            if (ActivityCompat.shouldShowRequestPermissionRationale(
                    context as Activity,
                    Manifest.permission.ACCESS_FINE_LOCATION
                )
            ) {

                AlertDialog.Builder(context)
                    .setTitle(context.getString(R.string.permission))
                    .setMessage(context.getString(R.string.add_location_permission))
                    .setPositiveButton(
                        context.getString(R.string.ok)
                    ) { _, _ ->
                        ActivityCompat.requestPermissions(
                            context,
                            arrayOf(Manifest.permission.ACCESS_FINE_LOCATION),
                            MapViewModel.MY_PERMISSIONS_REQUEST_LOCATION
                        )
                    }
                    .create()
                    .show()

            } else {
                ActivityCompat.requestPermissions(
                    context,
                    arrayOf(Manifest.permission.ACCESS_FINE_LOCATION),
                    MapViewModel.MY_PERMISSIONS_REQUEST_LOCATION
                )
            }
        }
    }
}