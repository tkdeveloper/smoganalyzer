package com.ath.smoganalyzer.repositories.sharedPreferences

import android.content.SharedPreferences
import com.ath.smoganalyzer.application.Keys.KEY_DOWNLOAD_COMPLETED
import com.ath.smoganalyzer.application.Keys.KEY_FIRST_TIME
import com.ath.smoganalyzer.application.Keys.KEY_STATION_HOME
import com.ath.smoganalyzer.application.extensions.KotlinExtension.editSharedPreferences
import io.reactivex.rxjava3.core.Completable
import io.reactivex.rxjava3.core.Observable
import io.reactivex.rxjava3.subjects.BehaviorSubject

class SharedPreferenceRealRepository(sharedPreferences: SharedPreferences) :
    SharedPreferencesRepository {

    private val prefSubject = BehaviorSubject.createDefault(sharedPreferences)
    private val prefChangeListener =
        SharedPreferences.OnSharedPreferenceChangeListener { sharedPreferences, _ ->
            prefSubject.onNext(sharedPreferences)
        }

    override fun setDownloadCompleted(): Completable = prefSubject
        .firstOrError()
        .editSharedPreferences {
            putBoolean(KEY_DOWNLOAD_COMPLETED, true)
        }

    override fun setSaveFirstTime(): Completable = prefSubject
        .firstOrError()
        .editSharedPreferences {
            putBoolean(KEY_FIRST_TIME, false)
        }

    override fun getDownloadCompleted(): Observable<Boolean> = prefSubject
        .map {
            it.getBoolean(KEY_DOWNLOAD_COMPLETED, false)
        }

    override fun checkIfFirstTime(): Observable<Boolean> = prefSubject
        .map {
            it.getBoolean(KEY_FIRST_TIME, true)
        }

    override fun saveHomeActiveStation(title: String): Completable = prefSubject
        .firstOrError()
        .editSharedPreferences {
            putString(KEY_STATION_HOME, title)
        }

    override fun getHomeActiveStation(): Observable<String> = prefSubject
        .map {
            it.getString(KEY_STATION_HOME, "")
        }

    init {
        sharedPreferences.registerOnSharedPreferenceChangeListener(prefChangeListener)
    }
}