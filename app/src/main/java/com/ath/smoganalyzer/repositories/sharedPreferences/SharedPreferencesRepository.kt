package com.ath.smoganalyzer.repositories.sharedPreferences

import io.reactivex.rxjava3.core.Completable
import io.reactivex.rxjava3.core.Observable

interface SharedPreferencesRepository {

    fun setDownloadCompleted(): Completable

    fun setSaveFirstTime(): Completable

    fun checkIfFirstTime(): Observable<Boolean>

    fun saveHomeActiveStation(title: String): Completable

    fun getHomeActiveStation(): Observable<String>

    fun getDownloadCompleted(): Observable<Boolean>
}