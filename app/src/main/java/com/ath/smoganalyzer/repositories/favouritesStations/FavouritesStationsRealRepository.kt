package com.ath.smoganalyzer.repositories.favouritesStations

import android.content.Context
import android.os.AsyncTask
import com.ath.smoganalyzer.database.FavouriteStationDao
import com.ath.smoganalyzer.database.StationDatabase
import com.ath.smoganalyzer.model.FavouriteStation
import com.ath.smoganalyzer.ui.favourites.OnFavouritesDataFetched

class FavouritesStationsRealRepository(context: Context) : FavouritesStationsRepository {

    private var favouriteStationDao: FavouriteStationDao

    init {
        val database: StationDatabase = StationDatabase
            .getInstance(context)!!
        favouriteStationDao = database.favouritesDao()!!
    }

    override fun insert(favouriteStation: FavouriteStation) {
        InsertStationAsyncTask(favouriteStationDao).execute(favouriteStation)
    }

    override fun getAllFavouritesStations(onFavouritesDataFetched: OnFavouritesDataFetched) {
        GetAllFavouriteStationAsyncTask(favouriteStationDao, onFavouritesDataFetched).execute("")
    }

    override fun getStationByTitle(
        title: String,
        onFavouritesDataFetched: OnFavouritesDataFetched
    ) {
        GetFavouriteStationAsyncTask(favouriteStationDao, onFavouritesDataFetched).execute(title)
    }

    override fun removeFromFavourites(favouriteStation: FavouriteStation) {
        RemoveStationAsyncTask(favouriteStationDao).execute(favouriteStation)
    }

    private class InsertStationAsyncTask(private val favouriteStationDao: FavouriteStationDao) :
        AsyncTask<FavouriteStation, Unit, Unit>() {
        override fun doInBackground(vararg p0: FavouriteStation?) {
            favouriteStationDao.insertFavStation(p0[0]!!)
        }
    }

    private class RemoveStationAsyncTask(private val favouriteStationDao: FavouriteStationDao) :
        AsyncTask<FavouriteStation, Unit, Unit>() {
        override fun doInBackground(vararg p0: FavouriteStation?) {
            favouriteStationDao.deleteStationFromFavourites(p0[0]!!.stationTitle)
        }
    }

    private class GetFavouriteStationAsyncTask(
        private val favouriteStationDao: FavouriteStationDao,
        private val onFavouritesDataFetched: OnFavouritesDataFetched
    ) :
        AsyncTask<String, Unit, FavouriteStation>() {
        override fun doInBackground(vararg p0: String): FavouriteStation {
            return favouriteStationDao.findByStationName(p0[0])
        }

        override fun onPostExecute(result: FavouriteStation?) {
            result?.let { onFavouritesDataFetched.onDataPassed(it) }
        }
    }

    private class GetAllFavouriteStationAsyncTask(
        private val favouriteStationDao: FavouriteStationDao,
        private val onFavouritesDataFetched: OnFavouritesDataFetched
    ) :
        AsyncTask<String, Unit, List<FavouriteStation>>() {
        override fun doInBackground(vararg p0: String?): List<FavouriteStation> {
            return favouriteStationDao.getAllFavouriteStations()
        }

        override fun onPostExecute(result: List<FavouriteStation>?) {
            result?.let { onFavouritesDataFetched.onDataPassed(it) }
        }
    }
}