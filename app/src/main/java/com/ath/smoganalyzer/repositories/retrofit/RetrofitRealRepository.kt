package com.ath.smoganalyzer.repositories.retrofit

import com.ath.smoganalyzer.api.ApiService
import com.ath.smoganalyzer.model.Station
import com.ath.smoganalyzer.model.StationSensors
import com.ath.smoganalyzer.model.StationSensorsAllDataUI
import com.ath.smoganalyzer.repositories.stations.StationsRealRepository
import com.ath.smoganalyzer.ui.activities.station.OnApiDataFetched
import com.orhanobut.logger.Logger
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.Disposable
import io.reactivex.schedulers.Schedulers

class RetrofitRealRepository(private val stationSensors: StationsRealRepository) :
    RetrofitRepository {

    private var stationsList: List<Station> = ArrayList()
    private var disposable: Disposable? = null
    private val apiService by lazy {
        ApiService.create()
    }

    override fun downloadAllSensors() {
        disposable = apiService.getStations()
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .doOnComplete {
                stationSensors.insert(stationsList)
            }
            .subscribe(
                { result ->
                    Logger.d(result.size)
                    stationsList = result
                },
                { error -> Logger.d(error) }
            )
    }

    override fun downloadInfoOneStation(id: Int, activity: OnApiDataFetched) {
        var sensorsInOneStation = arrayListOf<StationSensors>()
        disposable = apiService.getStationSensors(id.toString())
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .doOnComplete {
                sensorsInOneStation.forEach {
                    downloadDataFromOneSensor(it, activity)
                }
            }
            .subscribe(
                { result ->
                    sensorsInOneStation = result

                },
                { error -> Logger.d(error) }
            )
    }

    override fun downloadDataFromOneSensor(
        stationSensors: StationSensors,
        activity: OnApiDataFetched
    ) {
        disposable = apiService.getStationSensorsValues(stationSensors.id.toString())
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe(
                { result ->
                    val stationSensorsAllDataUI = StationSensorsAllDataUI(stationSensors, result)
                    activity.dataSensorsPassed(stationSensorsAllDataUI)
                },
                { error -> Logger.d(error) }
            )
    }
}