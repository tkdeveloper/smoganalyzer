package com.ath.smoganalyzer.repositories.permission

import android.content.Context

interface PermissionRepository {
    fun checkBasicPermissionForMap(context: Context)
}