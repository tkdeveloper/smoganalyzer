package com.ath.smoganalyzer.repositories.stations

import android.content.Context
import android.os.AsyncTask
import androidx.lifecycle.LiveData
import com.ath.smoganalyzer.database.StationDao
import com.ath.smoganalyzer.database.StationDatabase
import com.ath.smoganalyzer.model.Station
import com.ath.smoganalyzer.ui.map.OnDatabaseDataFetched

class StationsRealRepository(context: Context) : StationsRepository {

    private var stationDao: StationDao

    private var allSensors: LiveData<List<Station>>

    init {
        val database: StationDatabase = StationDatabase
            .getInstance(context)!!
        stationDao = database.stationDao()!!
        allSensors = stationDao.getAllStations()
    }

    override fun insert(station: Station) {
        InsertStationAsyncTask(
            stationDao
        ).execute(station)
    }

    override fun insert(stations: List<Station>) {
        InsertMultipleStationsAsyncTask(
            stationDao
        ).execute(stations)
    }

    override fun getAllStations(): LiveData<List<Station>> {
        return allSensors
    }

    override fun getStationByStationName(
        titleIntent: String?,
        onDatabaseDataFetched: OnDatabaseDataFetched
    ) {
        GetStationAsyncTask(stationDao, onDatabaseDataFetched).execute(titleIntent)
    }

    private class InsertStationAsyncTask(private val stationDao: StationDao) :
        AsyncTask<Station, Unit, Unit>() {
        override fun doInBackground(vararg p0: Station?) {
            stationDao.insertStation(p0[0]!!)
        }
    }

    private class InsertMultipleStationsAsyncTask(private val stationDao: StationDao) :
        AsyncTask<List<Station>, Unit, Unit>() {
        override fun doInBackground(vararg p0: List<Station>?) {
            stationDao.insertStation(p0[0]!!)
        }
    }

    private class GetStationAsyncTask(
        private val stationDao: StationDao,
        private val onDatabaseDataFetched: OnDatabaseDataFetched
    ) :
        AsyncTask<String, Unit, Station>() {
        override fun doInBackground(vararg p0: String): Station {
            return stationDao.findByStationName(p0[0])
        }

        override fun onPostExecute(result: Station?) {
            result?.let { onDatabaseDataFetched.onDataPassed(it) }
        }
    }
}