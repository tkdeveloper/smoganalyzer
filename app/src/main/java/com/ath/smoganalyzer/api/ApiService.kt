package com.ath.smoganalyzer.api

import com.ath.smoganalyzer.model.Station
import com.ath.smoganalyzer.model.StationSensorData
import com.ath.smoganalyzer.model.StationSensors
import com.google.gson.GsonBuilder
import io.reactivex.Observable
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory
import retrofit2.http.GET
import retrofit2.http.Path

interface ApiService {
    @GET("rest/station/findAll")
    fun getStations(): Observable<ArrayList<Station>>

    @GET("rest/station/sensors/{stationId}")
    fun getStationSensors(@Path("stationId") id: String): Observable<ArrayList<StationSensors>>

    @GET("rest/data/getData/{sensorId}")
    fun getStationSensorsValues(@Path("sensorId") id: String): Observable<StationSensorData>

    companion object {
        fun create(): ApiService {
            val interceptor = HttpLoggingInterceptor()
            interceptor.level = HttpLoggingInterceptor.Level.BODY
            val client = OkHttpClient.Builder().addInterceptor(interceptor).build()

            val gson = GsonBuilder()
                .setLenient()
                .create()

            val retrofit = Retrofit.Builder()
                .client(client)
                .addCallAdapterFactory(
                    RxJava2CallAdapterFactory.create()
                )
                .addConverterFactory(
                    GsonConverterFactory.create(gson)
                )
                .baseUrl(Endpoints.SERVICE_URL)
                .build()

            return retrofit.create(ApiService::class.java)
        }
    }
}