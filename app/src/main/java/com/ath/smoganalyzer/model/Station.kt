package com.ath.smoganalyzer.model

import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity(tableName = "station")
data class Station(
    @PrimaryKey var id: Int, var stationName: String, var gegrLat: String, var gegrLon: String
)