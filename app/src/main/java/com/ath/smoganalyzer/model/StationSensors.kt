package com.ath.smoganalyzer.model

data class StationSensors(var id: Int, var stationId: String, var param: StationSensorsParam)