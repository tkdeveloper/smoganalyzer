package com.ath.smoganalyzer.model

data class StationSensorsParam(
    var paramName: String, var paramFormula: String,
    var paramCode: String, var idParam: Int
)