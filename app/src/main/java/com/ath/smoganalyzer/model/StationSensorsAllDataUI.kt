package com.ath.smoganalyzer.model

data class StationSensorsAllDataUI(
    var stationSensors: StationSensors?,
    var values: StationSensorData?
)