package com.ath.smoganalyzer.model

import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity(tableName = "favouriteStation")
data class FavouriteStation(@PrimaryKey var id: Int, var stationTitle: String)