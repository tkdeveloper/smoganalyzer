package com.ath.smoganalyzer.model

data class SensorData(var date: String, var value: String)