package com.ath.smoganalyzer.model

data class StationSensorData(var key: String, var values: List<SensorData>)