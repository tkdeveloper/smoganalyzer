package com.ath.smoganalyzer.di.modules

import com.ath.smoganalyzer.ui.activities.main.MainActivity
import com.ath.smoganalyzer.ui.activities.station.StationActivity
import dagger.Module
import dagger.android.ContributesAndroidInjector

@Module
interface ActivityModule {

    @ContributesAndroidInjector
    fun bindMainActivity(): MainActivity

    @ContributesAndroidInjector
    fun bindSensorActivity(): StationActivity
}