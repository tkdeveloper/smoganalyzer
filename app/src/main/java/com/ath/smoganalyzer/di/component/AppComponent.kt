package com.ath.smoganalyzer.di.component

import com.ath.smoganalyzer.application.BaseApplication
import com.ath.smoganalyzer.di.modules.ActivityModule
import com.ath.smoganalyzer.di.modules.AppModule
import com.ath.smoganalyzer.di.modules.ViewModelModule
import com.ath.smoganalyzer.ui.home.HomeFragment
import dagger.Component
import dagger.android.AndroidInjectionModule
import javax.inject.Singleton

@Singleton
@Component(
    modules = [
        AndroidInjectionModule::class,
        AppModule::class,
        ActivityModule::class,
        ViewModelModule::class
    ]
)
interface AppComponent {
    fun inject(app: BaseApplication)
    fun inject(fragment: HomeFragment)
}
