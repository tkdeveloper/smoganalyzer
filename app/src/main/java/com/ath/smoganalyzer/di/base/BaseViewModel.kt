package com.ath.smoganalyzer.di.base

import androidx.lifecycle.ViewModel

abstract class BaseViewModel : ViewModel()