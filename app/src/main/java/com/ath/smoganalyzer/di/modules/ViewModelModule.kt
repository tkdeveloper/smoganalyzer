package com.ath.smoganalyzer.di.modules

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.ath.smoganalyzer.application.ViewModelFactory
import com.ath.smoganalyzer.di.annotation.ViewModelKey
import com.ath.smoganalyzer.ui.activities.main.MainActivityViewModel
import com.ath.smoganalyzer.ui.activities.station.StationActivityViewModel
import com.ath.smoganalyzer.ui.favourites.FavouritesViewModel
import com.ath.smoganalyzer.ui.home.HomeViewModel
import com.ath.smoganalyzer.ui.map.MapViewModel
import dagger.Binds
import dagger.Module
import dagger.multibindings.IntoMap

@Module
interface ViewModelModule {

    @Binds
    fun bindViewModelFactory(viewModelFactory: ViewModelFactory): ViewModelProvider.Factory

    @Binds
    @IntoMap
    @ViewModelKey(FavouritesViewModel::class)
    fun bindFavouritesViewModel(viewModel: FavouritesViewModel): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(HomeViewModel::class)
    fun bindHomeViewModel(viewModel: HomeViewModel): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(MapViewModel::class)
    fun bindMapViewModel(viewModel: MapViewModel): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(MainActivityViewModel::class)
    fun bindMainActivityViewModel(viewModel: MainActivityViewModel): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(StationActivityViewModel::class)
    fun bindSensorActivityViewModel(viewModel: StationActivityViewModel): ViewModel
}
