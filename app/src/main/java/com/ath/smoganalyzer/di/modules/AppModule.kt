package com.ath.smoganalyzer.di.modules

import android.app.Application
import android.content.Context
import android.content.SharedPreferences
import androidx.fragment.app.FragmentActivity
import androidx.preference.PreferenceManager
import com.ath.smoganalyzer.repositories.favouritesStations.FavouritesStationsRealRepository
import com.ath.smoganalyzer.repositories.favouritesStations.FavouritesStationsRepository
import com.ath.smoganalyzer.repositories.retrofit.RetrofitRealRepository
import com.ath.smoganalyzer.repositories.retrofit.RetrofitRepository
import com.ath.smoganalyzer.repositories.sharedPreferences.SharedPreferenceRealRepository
import com.ath.smoganalyzer.repositories.sharedPreferences.SharedPreferencesRepository
import com.ath.smoganalyzer.repositories.stations.StationsRealRepository
import com.ath.smoganalyzer.repositories.stations.StationsRepository
import com.tbruyelle.rxpermissions2.RxPermissions
import dagger.Module
import dagger.Provides
import kotlinx.coroutines.Dispatchers
import javax.inject.Singleton
import kotlin.coroutines.CoroutineContext

@Module
class AppModule(private val context: Context) {

    @Provides
    @Singleton
    fun provideRetrofitRepository(): RetrofitRepository =
        RetrofitRealRepository(StationsRealRepository(context))

    @Provides
    @Singleton
    fun provideStationsRepository(): StationsRepository = StationsRealRepository(context)

    @Provides
    @Singleton
    fun provideFavouriteStationsRepository(): FavouritesStationsRepository =
        FavouritesStationsRealRepository(context)

    @Provides
    @Singleton
    fun provideCoroutineContext(): CoroutineContext {
        return Dispatchers.Main
    }

    @Provides
    @Singleton
    fun provideSharedPreferencesRealRepository(): SharedPreferencesRepository =
        SharedPreferenceRealRepository(
            context.getSharedPreferences(
                "PrefSmogRef",
                Context.MODE_PRIVATE
            )
        )

    @Provides
    @Singleton
    fun provideRxPermissions(): RxPermissions {
        return RxPermissions(context as FragmentActivity)
    }

    @Provides
    @Singleton
    fun providesSharedPreferences(application: Application): SharedPreferences? =
        PreferenceManager.getDefaultSharedPreferences(application)
}


