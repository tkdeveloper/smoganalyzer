package com.ath.smoganalyzer.di.base

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import com.orhanobut.logger.AndroidLogAdapter
import com.orhanobut.logger.Logger
import dagger.android.AndroidInjection


abstract class BaseActivity : AppCompatActivity(), BaseView {

    protected lateinit var baseViewModel: BaseViewModel

    abstract val layoutId: Int

    protected abstract fun initializeViewModel()

    override fun onCreate(savedInstanceState: Bundle?) {
        AndroidInjection.inject(this)
        super.onCreate(savedInstanceState)
        setContentView(layoutId)
        Logger.addLogAdapter(AndroidLogAdapter())
        initializeViewModel()
        observeViewModel()
    }

    abstract fun observeViewModel()
}
