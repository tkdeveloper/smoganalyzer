package com.ath.smoganalyzer.ui.activities.main

import android.os.Bundle
import androidx.navigation.findNavController
import androidx.navigation.ui.AppBarConfiguration
import androidx.navigation.ui.setupActionBarWithNavController
import androidx.navigation.ui.setupWithNavController
import com.ath.smoganalyzer.R
import com.ath.smoganalyzer.application.ViewModelFactory
import com.ath.smoganalyzer.di.base.BaseActivity
import com.ath.smoganalyzer.repositories.retrofit.RetrofitRepository
import com.ath.smoganalyzer.repositories.sharedPreferences.SharedPreferencesRepository
import com.google.android.material.bottomnavigation.BottomNavigationView
import javax.inject.Inject

class MainActivity : BaseActivity() {

    @Inject
    lateinit var viewModel: MainActivityViewModel

    @Inject
    lateinit var viewModelFactory: ViewModelFactory

    @Inject
    lateinit var retrofitRepository: RetrofitRepository

    @Inject
    lateinit var sharedPreferencesRepository: SharedPreferencesRepository

    override val layoutId: Int
        get() = R.layout.activity_main


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        initializeView()
        viewModel.fillDatabaseIfFirstTime(sharedPreferencesRepository, retrofitRepository)
    }

    override fun observeViewModel() {}

    override fun initializeViewModel() {
        viewModel = viewModelFactory.create(viewModel::class.java)
    }

    private fun initializeView() {
        val navView: BottomNavigationView = findViewById(R.id.nav_view)

        val navController = findNavController(R.id.nav_host_fragment)
        val appBarConfiguration = AppBarConfiguration(
            setOf(
                R.id.navigation_home, R.id.navigation_map, R.id.navigation_favourites
            )
        )
        setupActionBarWithNavController(navController, appBarConfiguration)
        navView.setupWithNavController(navController)
    }
}
