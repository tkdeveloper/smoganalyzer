package com.ath.smoganalyzer.ui.activities.station

import com.ath.smoganalyzer.model.StationSensorsAllDataUI

interface OnApiDataFetched {
    fun dataSensorsPassed(stationSensorsAllDataUI: StationSensorsAllDataUI)
}