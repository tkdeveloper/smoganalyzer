package com.ath.smoganalyzer.ui.activities.station.recycler;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.ath.smoganalyzer.R;
import com.ath.smoganalyzer.model.SensorData;
import com.ath.smoganalyzer.model.StationSensorData;
import com.ath.smoganalyzer.model.StationSensorsAllDataUI;
import com.ath.smoganalyzer.model.StationSensorsParam;

import java.util.ArrayList;


public class SensorAdapter extends RecyclerView.Adapter {
    // źródło danych
    private ArrayList<StationSensorsAllDataUI> StationSensorData = new ArrayList<>();

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup viewGroup, final int i) {
        View view = LayoutInflater.from(viewGroup.getContext())
                .inflate(R.layout.content_sensor_item, viewGroup, false);

        return new SensorDataViewHolder(view);
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder viewHolder, final int i) {
        if (StationSensorData.size() == 0)
            return;

        StationSensorsAllDataUI allDataUI = StationSensorData.get(i);
        StationSensorsParam dataStation = allDataUI.getStationSensors().getParam();
        StationSensorData dataSensors = allDataUI.getValues();

        double averageValue = 0.0;
        for (SensorData item : dataSensors.getValues()) {
            try {
                averageValue += Double.parseDouble(item.getValue());
            } catch (Exception e) {
                Log.d("ExceptionSmog", "Exc: " + e);
            }
        }
        averageValue = averageValue / dataSensors.getValues().size();

        ((SensorDataViewHolder) viewHolder).paramName.setText(dataStation.getParamName());
        ((SensorDataViewHolder) viewHolder).paramCode.setText(dataStation.getParamCode());
        ((SensorDataViewHolder) viewHolder).currentValue.setText(getCurrentValueFormat(dataSensors
                .getValues()
                .get(1)
                .getValue()));
        ((SensorDataViewHolder) viewHolder).average.setText(String.valueOf(averageValue));
    }

    private String getCurrentValueFormat(String value) {
        if (value == null) return "----";
        return value;
    }

    @Override
    public int getItemCount() {
        return StationSensorData.size();
    }

    private static class SensorDataViewHolder extends RecyclerView.ViewHolder {
        TextView paramName;
        TextView paramCode;
        TextView currentValue;
        TextView average;

        SensorDataViewHolder(View pItem) {
            super(pItem);
            paramName = pItem.findViewById(R.id.paramNameTV);
            paramCode = pItem.findViewById(R.id.paramCodeTV);
            currentValue = pItem.findViewById(R.id.currentValueTV);
            average = pItem.findViewById(R.id.averTV);
        }
    }

    public void addNewSensorData(StationSensorsAllDataUI stationSensorsAllDataUI) {
        StationSensorData.add(stationSensorsAllDataUI);
    }
}
