package com.ath.smoganalyzer.ui.favourites

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.ath.smoganalyzer.repositories.favouritesStations.FavouritesStationsRepository
import javax.inject.Inject

class FavouritesViewModel @Inject constructor() : ViewModel() {


    fun loadDataFromDatabase(
        favouritesStationsRepository: FavouritesStationsRepository,
        onFavouritesDataFetched: OnFavouritesDataFetched
    ) {
        favouritesStationsRepository.getAllFavouritesStations(onFavouritesDataFetched)
    }

    private val _text = MutableLiveData<String>().apply {
        value = "This is notifications Fragment"
    }
    val text: LiveData<String> = _text
}