package com.ath.smoganalyzer.ui.activities.station

import android.os.Bundle
import android.view.Menu
import android.view.MenuItem
import android.view.View
import androidx.appcompat.widget.Toolbar
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.ath.smoganalyzer.R
import com.ath.smoganalyzer.application.Keys.KEY_SENSOR_TITLE
import com.ath.smoganalyzer.application.ViewModelFactory
import com.ath.smoganalyzer.di.base.BaseActivity
import com.ath.smoganalyzer.model.FavouriteStation
import com.ath.smoganalyzer.model.Station
import com.ath.smoganalyzer.model.StationSensorsAllDataUI
import com.ath.smoganalyzer.repositories.favouritesStations.FavouritesStationsRepository
import com.ath.smoganalyzer.repositories.retrofit.RetrofitRepository
import com.ath.smoganalyzer.repositories.sharedPreferences.SharedPreferencesRepository
import com.ath.smoganalyzer.repositories.stations.StationsRepository
import com.ath.smoganalyzer.ui.activities.station.recycler.SensorAdapter
import com.ath.smoganalyzer.ui.favourites.OnFavouritesDataFetched
import com.ath.smoganalyzer.ui.map.OnDatabaseDataFetched
import io.reactivex.rxjava3.schedulers.Schedulers
import javax.inject.Inject


class StationActivity : BaseActivity(), OnDatabaseDataFetched, OnApiDataFetched,
    OnFavouritesDataFetched {

    @Inject
    lateinit var viewModel: StationActivityViewModel

    @Inject
    lateinit var viewModelFactory: ViewModelFactory

    @Inject
    lateinit var retrofitRepository: RetrofitRepository

    @Inject
    lateinit var stationsRepository: StationsRepository

    @Inject
    lateinit var favouritesStationsRepository: FavouritesStationsRepository

    @Inject
    lateinit var sharedPreferencesRepository: SharedPreferencesRepository

    private lateinit var recyclerView: RecyclerView

    private lateinit var sensorAdapter: SensorAdapter

    private lateinit var toolbar: Toolbar

    private lateinit var menu: Menu

    private var isFavourite = false

    private var isHomeSet = false

    private var currentFavStation: FavouriteStation? = null

    override val layoutId: Int
        get() = R.layout.activity_sensor

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        initViewComponents()
        handleIntent()
    }

    override fun onSupportNavigateUp(): Boolean {
        onBackPressed()
        return true
    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        menuInflater.inflate(R.menu.station_menu, menu)
        this.menu = menu!!

        if (isFavourite)
            setFavouriteIconEnabled()


        runOnUiThread {
            sharedPreferencesRepository.getHomeActiveStation().subscribe {
                if (it == viewModel.getCurrentStation().stationName)
                    setHomeStationIconEnabled()
                else
                    setHomeStationDisabled()
            }
        }

        return true
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        val id: Int = item.itemId
        if (id == R.id.action_favorite) {

            when (isFavourite) {
                true -> {
                    if (currentFavStation == null)
                        return true
                    setFavouriteIconDisabled()
                    viewModel.removeFromFavourites(
                        currentFavStation!!,
                        favouritesStationsRepository
                    )
                }
                false -> {
                    setFavouriteIconEnabled()
                    viewModel.saveToFavouritesInDb(favouritesStationsRepository)

                }
            }
            return true
        }

        if (id == R.id.action_home) {
            val currentStation = viewModel.getCurrentStation()

            if (isHomeSet) {
                sharedPreferencesRepository.saveHomeActiveStation("")
                    .subscribeOn(Schedulers.io()).subscribe()
                setHomeStationDisabled()
            } else {
                sharedPreferencesRepository.saveHomeActiveStation(currentStation.stationName)
                    .subscribeOn(Schedulers.io()).subscribe()
                setHomeStationIconEnabled()
            }
        }

        return super.onOptionsItemSelected(item)
    }

    override fun observeViewModel() {}

    override fun initializeViewModel() {
        viewModel = viewModelFactory.create(viewModel::class.java)
    }

    override fun onDataPassed(station: Station) {
        viewModel.passSensorToGetCurrentData(station, retrofitRepository, this)
        viewModel.checkIfIsFavourite(station, favouritesStationsRepository, this)
    }

    override fun dataSensorsPassed(stationSensorsAllDataUI: StationSensorsAllDataUI) {
        viewModel.addNewSensorDataToRecycler(sensorAdapter, stationSensorsAllDataUI)
    }

    override fun onDataPassed(favourite: FavouriteStation) {
        isFavourite = true
        currentFavStation = favourite
    }

    override fun onDataPassed(favourites: List<FavouriteStation>) {
    }

    private fun initViewComponents() {
        toolbar = findViewById<View>(R.id.toolbar) as Toolbar
        setSupportActionBar(toolbar)
        supportActionBar!!.setDisplayHomeAsUpEnabled(true)
        supportActionBar!!.setDisplayShowHomeEnabled(true)

        recyclerView = findViewById(R.id.content_sensor_RV)
        recyclerView.setHasFixedSize(true)
        recyclerView.layoutManager = LinearLayoutManager(this)

        sensorAdapter = SensorAdapter()
        recyclerView.adapter = sensorAdapter
    }

    private fun setFavouriteIconEnabled() {
        menu.findItem(R.id.action_favorite).icon =
            ContextCompat.getDrawable(this, R.drawable.baseline_star_white_48dp)
    }

    private fun setFavouriteIconDisabled() {
        menu.findItem(R.id.action_favorite).icon =
            ContextCompat.getDrawable(this, R.drawable.baseline_star_border_white_48dp)
    }

    private fun setHomeStationIconEnabled() {
        menu.findItem(R.id.action_home).icon =
            ContextCompat.getDrawable(this, R.drawable.ic_home_black_24dp)
        isHomeSet = true
    }

    private fun setHomeStationDisabled() {
        menu.findItem(R.id.action_home).icon =
            ContextCompat.getDrawable(this, R.drawable.ic_home_white_24dp)
        isHomeSet = false
    }

    private fun handleIntent() {
        val intent = intent
        val titleIntent = intent.getStringExtra(KEY_SENSOR_TITLE)
        title = titleIntent
        stationsRepository.getStationByStationName(titleIntent, this)
    }
}
