package com.ath.smoganalyzer.ui.home

import android.content.Context
import android.graphics.drawable.Drawable
import android.view.View
import androidx.lifecycle.ViewModel
import com.ath.smoganalyzer.R
import com.ath.smoganalyzer.model.Station
import com.ath.smoganalyzer.model.StationSensorsAllDataUI
import com.ath.smoganalyzer.repositories.retrofit.RetrofitRepository
import com.ath.smoganalyzer.ui.activities.station.OnApiDataFetched
import javax.inject.Inject

class HomeViewModel @Inject constructor() : ViewModel() {

    private lateinit var currentStation: Station

    fun passSensorToGetCurrentData(
        station: Station,
        retrofitRepository: RetrofitRepository,
        homeFragment: OnApiDataFetched
    ) {
        currentStation = station
        retrofitRepository.downloadInfoOneStation(station.id, homeFragment)
    }

    fun getCurrentStation(): Station {
        return currentStation
    }

    fun calculateAirPollution(
        root: View,
        stationSensorsAllDataUI: StationSensorsAllDataUI,
        context: Context?
    ) {
        val value = stationSensorsAllDataUI.values!!.values[2].value.toFloat()

        if (value < 30f) {
            setGoodAirQuality(root, context)
            return
        }

        if (value in 30f..69f) {
            setMediumAirQuality(root, context)
            return
        }

        setBadAirQuality(root, context)
    }

    private fun setGoodAirQuality(root: View, context: Context?) {
        val backgroundColor = context?.getDrawable(R.drawable.green_background)
        val priority = context?.getDrawable(R.drawable.beenhere_24dp)
        val airText = context?.getDrawable(R.drawable.border_green)
        val backView = context?.getDrawable(R.drawable.ellipse_2_green)
        updateView(root, backgroundColor, priority, airText, backView)
    }

    private fun setMediumAirQuality(root: View, context: Context?) {
        val backgroundColor = context?.getDrawable(R.drawable.yellow_background)
        val priority = context?.getDrawable(R.drawable.sentiment_24px)
        val airText = context?.getDrawable(R.drawable.border_yellow)
        val backView = context?.getDrawable(R.drawable.ellipse_2_yellow)
        updateView(root, backgroundColor, priority, airText, backView)
    }

    private fun setBadAirQuality(root: View, context: Context?) {
        val backgroundColor = context?.getDrawable(R.drawable.red_background)
        val priority = context?.getDrawable(R.drawable.priority_24px)
        val airText = context?.getDrawable(R.drawable.border_red)
        val backView = context?.getDrawable(R.drawable.ellipse_2_red)
        updateView(root, backgroundColor, priority, airText, backView)
    }

    private fun updateView(
        root: View,
        background: Drawable?,
        priority: Drawable?,
        airText: Drawable?,
        backView: Drawable?
    ) {
        root.background = background
        root.findViewById<View>(R.id.priority).background = priority
        root.findViewById<View>(R.id.air_text_city).background = airText
        root.findViewById<View>(R.id.ellipse_2_background).background = backView
    }
}