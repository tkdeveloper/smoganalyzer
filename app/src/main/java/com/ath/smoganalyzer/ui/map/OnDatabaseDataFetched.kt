package com.ath.smoganalyzer.ui.map

import com.ath.smoganalyzer.model.Station

interface OnDatabaseDataFetched {
    fun onDataPassed(station: Station)
}