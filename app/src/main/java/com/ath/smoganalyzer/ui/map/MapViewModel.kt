package com.ath.smoganalyzer.ui.map

import android.Manifest
import android.content.Context
import android.content.Intent
import android.content.pm.PackageManager
import android.location.Location
import android.os.Build
import android.os.Bundle
import android.os.Looper
import androidx.core.content.ContextCompat
import androidx.lifecycle.LiveData
import androidx.lifecycle.ViewModel
import com.ath.smoganalyzer.R
import com.ath.smoganalyzer.application.Keys.KEY_SENSOR_TITLE
import com.ath.smoganalyzer.model.Station
import com.ath.smoganalyzer.repositories.permission.PermissionRealRepository
import com.ath.smoganalyzer.repositories.stations.StationsRealRepository
import com.ath.smoganalyzer.repositories.stations.StationsRepository
import com.ath.smoganalyzer.ui.activities.station.StationActivity
import com.google.android.gms.location.*
import com.google.android.gms.maps.*
import com.google.android.gms.maps.model.BitmapDescriptorFactory
import com.google.android.gms.maps.model.LatLng
import com.google.android.gms.maps.model.Marker
import com.google.android.gms.maps.model.MarkerOptions
import com.orhanobut.logger.Logger
import javax.inject.Inject


open class MapViewModel @Inject constructor() : ViewModel(), OnMapReadyCallback,
    GoogleMap.OnMarkerClickListener {

    private lateinit var context: Context
    private lateinit var stationsRepository: StationsRepository

    private var fusedLocationClient: FusedLocationProviderClient? = null
    private var lastLocation: Location? = null

    lateinit var locationRequest: LocationRequest
    lateinit var googleMap: GoogleMap

    internal var currLocationMarker: Marker? = null

    private var locationCallback: LocationCallback = object : LocationCallback() {
        override fun onLocationResult(locationResult: LocationResult) {
            val locationList = locationResult.locations
            if (locationList.isNotEmpty()) {
                val location = locationList.last()
                lastLocation = location
                if (currLocationMarker != null) {
                    currLocationMarker?.remove()
                }

                val latLng = LatLng(location.latitude, location.longitude)
                val markerOptions = MarkerOptions()
                markerOptions.position(latLng)
                markerOptions.title(context.getString(R.string.current_position))
                markerOptions.icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_MAGENTA))
                currLocationMarker = googleMap.addMarker(markerOptions)

                googleMap.moveCamera(CameraUpdateFactory.newLatLngZoom(latLng, 8.0F))
                googleMap.setOnMarkerClickListener(this@MapViewModel)
            }
        }
    }

    override fun onMarkerClick(p0: Marker?): Boolean {
        val intent = Intent(context, StationActivity::class.java)
        intent.putExtra(KEY_SENSOR_TITLE, p0?.title)
        context.startActivity(intent)
        return false
    }

    override fun onMapReady(map: GoogleMap?) {
        this.googleMap = map!!
        locationRequest = createLocationRequest()

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (ContextCompat.checkSelfPermission(
                    context,
                    Manifest.permission.ACCESS_FINE_LOCATION
                ) == PackageManager.PERMISSION_GRANTED
            ) {
                fusedLocationClient?.requestLocationUpdates(
                    locationRequest,
                    locationCallback,
                    Looper.myLooper()
                )
                googleMap.isMyLocationEnabled = true
            }
        } else {
            fusedLocationClient?.requestLocationUpdates(
                locationRequest,
                locationCallback,
                Looper.myLooper()
            )
            googleMap.isMyLocationEnabled = true

        }

        fusedLocationClient?.requestLocationUpdates(
            locationRequest,
            locationCallback,
            Looper.myLooper()
        )
    }

    fun attachContextAndMapFragment(
        context: Context,
        mapView: MapView,
        savedInstanceState: Bundle?
    ) {
        this.context = context
        mapView.onCreate(savedInstanceState)
        mapView.onResume()
        MapsInitializer.initialize(context)
        mapView.getMapAsync(this)
        fusedLocationClient = LocationServices.getFusedLocationProviderClient(context)

        val permissionRepository = PermissionRealRepository()
        permissionRepository.checkBasicPermissionForMap(context)
        stationsRepository = StationsRealRepository(context)
    }

    fun getAllSensors(): LiveData<List<Station>> {
        return stationsRepository.getAllStations()
    }

    companion object {
        const val MY_PERMISSIONS_REQUEST_LOCATION = 99
        const val LOCATION_REQUEST_INTERVAL = 120000
    }

    private fun createLocationRequest(): LocationRequest {
        locationRequest = LocationRequest()
        locationRequest.interval = LOCATION_REQUEST_INTERVAL.toLong()
        locationRequest.fastestInterval = LOCATION_REQUEST_INTERVAL.toLong()
        locationRequest.priority = LocationRequest.PRIORITY_BALANCED_POWER_ACCURACY

        return locationRequest
    }

    fun addSensorsToMap(stations: List<Station>?) {
        for (item in stations!!) {
            val marker = convertSensorIntoMarker(item)
            googleMap.addMarker(marker)
        }

        Logger.d(stations.size)
    }

    fun convertSensorIntoMarker(station: Station): MarkerOptions {
        return MarkerOptions().position(
            LatLng(
                station.gegrLat.toDouble(),
                station.gegrLon.toDouble()
            )
        ).title(station.stationName)

    }
}