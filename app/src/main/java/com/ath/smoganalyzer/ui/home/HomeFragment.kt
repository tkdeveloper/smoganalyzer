package com.ath.smoganalyzer.ui.home

import android.annotation.SuppressLint
import android.content.Context
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.ath.smoganalyzer.R
import com.ath.smoganalyzer.model.Station
import com.ath.smoganalyzer.model.StationSensorsAllDataUI
import com.ath.smoganalyzer.repositories.retrofit.RetrofitRealRepository
import com.ath.smoganalyzer.repositories.retrofit.RetrofitRepository
import com.ath.smoganalyzer.repositories.sharedPreferences.SharedPreferenceRealRepository
import com.ath.smoganalyzer.repositories.sharedPreferences.SharedPreferencesRepository
import com.ath.smoganalyzer.repositories.stations.StationsRealRepository
import com.ath.smoganalyzer.repositories.stations.StationsRepository
import com.ath.smoganalyzer.ui.activities.station.OnApiDataFetched
import com.ath.smoganalyzer.ui.home.recycler.HomeSensorsAdapter
import com.ath.smoganalyzer.ui.map.OnDatabaseDataFetched
import io.reactivex.rxjava3.schedulers.Schedulers
import kotlinx.android.synthetic.main.bad_air.*
import javax.inject.Inject

class HomeFragment : Fragment(), OnDatabaseDataFetched, OnApiDataFetched {

    @Inject
    lateinit var sharedPreferencesRepository: SharedPreferencesRepository

    @Inject
    lateinit var retrofitRepository: RetrofitRepository

    @Inject
    lateinit var stationsRepository: StationsRepository

    private lateinit var homeViewModel: HomeViewModel

    private lateinit var recyclerView: RecyclerView

    private lateinit var root: View

    private lateinit var adapter: HomeSensorsAdapter

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        root = inflater.inflate(R.layout.fragment_home, container, false)
        return root
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        context?.let { checkSharedPreferences(it) }
        initViewComponents()
    }

    override fun onDataPassed(station: Station) {
        homeViewModel = ViewModelProvider(this).get(HomeViewModel::class.java)
        val stationRepository = context?.let { StationsRealRepository(it) }
        retrofitRepository = stationRepository?.let { RetrofitRealRepository(it) }!!
        homeViewModel.passSensorToGetCurrentData(station, retrofitRepository, this)
    }

    override fun dataSensorsPassed(stationSensorsAllDataUI: StationSensorsAllDataUI) {
        val stationName = homeViewModel.getCurrentStation().stationName
        air_text_city.text = stationName
        adapter.addNewSensorData(stationSensorsAllDataUI)
        adapter.notifyDataSetChanged()
        adjustViewToData(stationSensorsAllDataUI)
        // finish download from api

    }

    @SuppressLint("SetTextI18n")
    private fun checkSharedPreferences(context: Context) {
        val sharedPreferencesRepository = SharedPreferenceRealRepository(
            context.getSharedPreferences(
                "PrefSmogRef",
                Context.MODE_PRIVATE
            )
        )
        sharedPreferencesRepository.getHomeActiveStation().subscribeOn(Schedulers.io())
            .subscribe {
                if (it == "") {
                    air_text_city.text = "Brak danych"
                } else {
                    stationsRepository = StationsRealRepository(context)
                    stationsRepository.getStationByStationName(it, this)
                }
            }
    }

    private fun initViewComponents() {
        recyclerView = root.findViewById(R.id.recycler_home)
        recyclerView.setHasFixedSize(true)
        recyclerView.layoutManager = LinearLayoutManager(root.context)

        adapter = HomeSensorsAdapter()
        recyclerView.adapter = adapter
    }

    private fun adjustViewToData(stationSensorsAllDataUI: StationSensorsAllDataUI) {
        if (stationSensorsAllDataUI.stationSensors!!.param.paramFormula != "PM10")
            return

        homeViewModel.calculateAirPollution(root, stationSensorsAllDataUI, context)
    }
}