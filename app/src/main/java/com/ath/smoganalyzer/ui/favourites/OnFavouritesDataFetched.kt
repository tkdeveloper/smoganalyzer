package com.ath.smoganalyzer.ui.favourites

import com.ath.smoganalyzer.model.FavouriteStation

interface OnFavouritesDataFetched {
    fun onDataPassed(favourite: FavouriteStation)
    fun onDataPassed(favourites: List<FavouriteStation>)
}