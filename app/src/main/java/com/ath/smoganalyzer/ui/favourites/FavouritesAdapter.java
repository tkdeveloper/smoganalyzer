package com.ath.smoganalyzer.ui.favourites;

import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.recyclerview.widget.RecyclerView;

import com.ath.smoganalyzer.R;
import com.ath.smoganalyzer.model.FavouriteStation;
import com.ath.smoganalyzer.ui.activities.station.StationActivity;

import java.util.ArrayList;
import java.util.List;

import static com.ath.smoganalyzer.application.Keys.KEY_SENSOR_TITLE;


public class FavouritesAdapter extends RecyclerView.Adapter {

    // źródło danych
    private ArrayList<FavouriteStation> favouriteStations = new ArrayList<>();

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup viewGroup, final int i) {
        View view = LayoutInflater.from(viewGroup.getContext())
                .inflate(R.layout.favourite_item_layout, viewGroup, false);
        return new MyViewHolder(view);
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder viewHolder, final int i) {
        FavouriteStation favouriteStation = favouriteStations.get(i);
        ((MyViewHolder) viewHolder).mTitle.setText(favouriteStation.getStationTitle());
        ((MyViewHolder) viewHolder).mainConFavItem.setOnClickListener(view -> {
            Intent intent = new Intent(view.getContext(), StationActivity.class);
            intent.putExtra(KEY_SENSOR_TITLE, favouriteStation.getStationTitle());
            view.getContext().startActivity(intent);
        });
    }

    @Override
    public int getItemCount() {
        return favouriteStations.size();
    }

    public void addSingleElement(FavouriteStation favouriteStation) {
        favouriteStations.add(favouriteStation);
    }

    public void removeDataFromList(FavouriteStation favouriteStation) {
        favouriteStations.remove(favouriteStation);
    }

    public void addMultipleItems(List<FavouriteStation> stations) {
        favouriteStations.addAll(stations);
    }

    public void removeAllData() {
        favouriteStations.clear();
        favouriteStations = new ArrayList<>();
    }

    public void reloadAllData(ArrayList<FavouriteStation> stations) {
        favouriteStations.clear();
        favouriteStations = stations;
    }

    // implementacja wzorca ViewHolder
    // każdy obiekt tej klasy przechowuje odniesienie do layoutu elementu listy
    // dzięki temu wywołujemy findViewById() tylko raz dla każdego elementu
    private class MyViewHolder extends RecyclerView.ViewHolder {
        public TextView mTitle;
        ConstraintLayout mainConFavItem;

        public MyViewHolder(View pItem) {
            super(pItem);
            mTitle = pItem.findViewById(R.id.station_title);
            mainConFavItem = pItem.findViewById(R.id.mainConFavItem);
        }
    }
}
