package com.ath.smoganalyzer.ui.favourites

import android.content.Context
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.ath.smoganalyzer.R
import com.ath.smoganalyzer.model.FavouriteStation
import com.ath.smoganalyzer.repositories.favouritesStations.FavouritesStationsRealRepository
import com.ath.smoganalyzer.repositories.favouritesStations.FavouritesStationsRepository
import javax.inject.Inject


class FavouritesFragment : Fragment(), OnFavouritesDataFetched {

    @Inject
    lateinit var factory: ViewModelProvider.Factory

    @Inject
    lateinit var favouritesStationsRepository: FavouritesStationsRepository

    private lateinit var viewModel: FavouritesViewModel

    private lateinit var recyclerView: RecyclerView

    private lateinit var root: View

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {

        root = inflater.inflate(R.layout.fragment_favourites, container, false)
        viewModel = ViewModelProvider(this).get(FavouritesViewModel::class.java)
        favouritesStationsRepository = FavouritesStationsRealRepository(root.context)
        viewModel.loadDataFromDatabase(favouritesStationsRepository, this)
        return root
    }

    override fun onResume() {
        super.onResume()
        viewModel.loadDataFromDatabase(favouritesStationsRepository, this)
    }

    override fun onDataPassed(favourite: FavouriteStation) {
        // Nothing would be received
    }

    override fun onDataPassed(favourites: List<FavouriteStation>) {
        initViewComponents(root, root.context, favourites)
    }

    private fun initViewComponents(
        root: View,
        context: Context,
        favourites: List<FavouriteStation>?
    ) {
        recyclerView = root.findViewById(R.id.stationsFavRV)
        recyclerView.setHasFixedSize(true)
        recyclerView.layoutManager = LinearLayoutManager(context)
        val adapter = FavouritesAdapter()
        adapter.addMultipleItems(favourites)
        adapter.notifyDataSetChanged()
        recyclerView.adapter = adapter
    }
}
