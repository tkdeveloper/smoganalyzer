package com.ath.smoganalyzer.ui.activities.main

import com.ath.smoganalyzer.di.base.BaseViewModel
import com.ath.smoganalyzer.repositories.retrofit.RetrofitRepository
import com.ath.smoganalyzer.repositories.sharedPreferences.SharedPreferencesRepository
import io.reactivex.rxjava3.schedulers.Schedulers
import javax.inject.Inject

class MainActivityViewModel @Inject constructor() : BaseViewModel() {

    fun fillDatabaseIfFirstTime(
        sharedPreferencesRepository: SharedPreferencesRepository,
        retrofitRepository: RetrofitRepository
    ) {
        sharedPreferencesRepository.checkIfFirstTime().subscribeOn(Schedulers.io())
            .subscribe { isFirstTime ->
                if (!isFirstTime)
                    return@subscribe
                downloadAllSensorsFromService(retrofitRepository)
                sharedPreferencesRepository.setSaveFirstTime().subscribe()
            }
    }

    private fun downloadAllSensorsFromService(retrofitRepository: RetrofitRepository) {
        retrofitRepository.downloadAllSensors()
    }
}