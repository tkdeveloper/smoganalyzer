package com.ath.smoganalyzer.ui.activities.station

import com.ath.smoganalyzer.di.base.BaseViewModel
import com.ath.smoganalyzer.model.FavouriteStation
import com.ath.smoganalyzer.model.Station
import com.ath.smoganalyzer.model.StationSensorsAllDataUI
import com.ath.smoganalyzer.repositories.favouritesStations.FavouritesStationsRepository
import com.ath.smoganalyzer.repositories.retrofit.RetrofitRepository
import com.ath.smoganalyzer.ui.activities.station.recycler.SensorAdapter
import com.ath.smoganalyzer.ui.favourites.OnFavouritesDataFetched
import javax.inject.Inject

class StationActivityViewModel @Inject constructor() : BaseViewModel() {

    private lateinit var currentStation: Station

    fun passSensorToGetCurrentData(
        station: Station,
        retrofitRepository: RetrofitRepository,
        stationActivity: OnApiDataFetched
    ) {
        currentStation = station
        retrofitRepository.downloadInfoOneStation(station.id, stationActivity)
    }

    fun getCurrentStation(): Station {
        return currentStation
    }

    fun addNewSensorDataToRecycler(
        sensorAdapter: SensorAdapter,
        stationSensorsAllDataUI: StationSensorsAllDataUI
    ) {
        sensorAdapter.addNewSensorData(stationSensorsAllDataUI)
        sensorAdapter.notifyDataSetChanged()
    }

    fun saveToFavouritesInDb(
        favouritesStationsRepository: FavouritesStationsRepository
    ) {
        val favouriteStation = FavouriteStation(currentStation.id, currentStation.stationName)
        favouritesStationsRepository.insert(favouriteStation)
    }

    fun checkIfIsFavourite(
        station: Station,
        favouritesStationsRepository: FavouritesStationsRepository,
        onFavouritesDataFetched: OnFavouritesDataFetched
    ) {
        favouritesStationsRepository.getStationByTitle(station.stationName, onFavouritesDataFetched)
    }

    fun removeFromFavourites(
        station: FavouriteStation,
        favouritesStationsRepository: FavouritesStationsRepository
    ) {
        favouritesStationsRepository.removeFromFavourites(station)
    }
}