package com.ath.smoganalyzer.application

object Keys {
    const val KEY_DOWNLOAD_COMPLETED = "downloadCompleted"
    const val KEY_FIRST_TIME = "firstTime"
    const val KEY_STATION_HOME = "stationHome"
    const val KEY_SENSOR_TITLE = "markerTitleKeyId"
}