package com.ath.smoganalyzer.database

import android.content.Context
import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase
import com.ath.smoganalyzer.model.FavouriteStation
import com.ath.smoganalyzer.model.Station


@Database(entities = [Station::class, FavouriteStation::class], version = 1)
abstract class StationDatabase : RoomDatabase() {
    abstract fun stationDao(): StationDao?
    abstract fun favouritesDao(): FavouriteStationDao?

    companion object {
        @Volatile
        private var INSTANCE: StationDatabase? = null
        fun getInstance(context: Context): StationDatabase? {
            if (INSTANCE == null) {
                synchronized(StationDatabase::class.java) {
                    if (INSTANCE == null) {
                        INSTANCE = Room.databaseBuilder(
                            context.applicationContext,
                            StationDatabase::class.java, "StationDatabase.db"
                        )
                            .build()
                    }
                }
            }
            return INSTANCE
        }
    }
}