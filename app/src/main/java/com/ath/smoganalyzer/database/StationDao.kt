package com.ath.smoganalyzer.database

import androidx.lifecycle.LiveData
import androidx.room.*
import com.ath.smoganalyzer.model.Station


@Dao
interface StationDao {

    @Update(onConflict = OnConflictStrategy.IGNORE)
    fun update(station: Station?)

    @Query("DELETE FROM station")
    fun deleteAll()

    @Query("SELECT * FROM station")
    fun getAllStations(): LiveData<List<Station>>

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertStation(stations: List<Station>?)

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertStation(station: Station?)

    @Query("SELECT * FROM station WHERE stationName LIKE :stationName LIMIT 1")
    fun findByStationName(stationName: String): Station
}