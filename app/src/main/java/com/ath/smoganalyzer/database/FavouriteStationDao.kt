package com.ath.smoganalyzer.database

import androidx.room.*
import com.ath.smoganalyzer.model.FavouriteStation

@Dao
interface FavouriteStationDao {

    @Update(onConflict = OnConflictStrategy.REPLACE)
    fun update(favouriteStation: FavouriteStation)

    @Query("DELETE FROM favouriteStation")
    fun deleteAllFavouritesStations()

    @Query("DELETE FROM favouriteStation WHERE stationTitle LIKE :stationTitle")
    fun deleteStationFromFavourites(stationTitle: String)

    @Query("SELECT * FROM favouriteStation")
    fun getAllFavouriteStations(): List<FavouriteStation>

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertFavStation(station: FavouriteStation?)

    @Query("SELECT * FROM favouriteStation WHERE stationTitle LIKE :stationTitle LIMIT 1")
    fun findByStationName(stationTitle: String): FavouriteStation
}